# LICENSES

## share-connect-support

FILES:

* `js/share-connect-support.min.js`
* `css/share-connect-support.css`

PROJECT: https://gitlab.com/distributopia/share-connect-support  
LICENSE: [GPLv3](https://gitlab.com/distributopia/share-connect-support/blob/master/LICENSE.md)
