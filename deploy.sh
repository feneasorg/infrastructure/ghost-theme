#!/bin/bash

token=$(curl -D /dev/null -H "Accept: application/json" \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -X POST -d "grant_type=password&username=$USER&password=$PASS&client_id=$CLIENT&client_secret=$CLIENTPASS" \
  https://feneas.org/ghost/api/v0.1/authentication/token | jq -r .access_token)

curl -f -D /dev/null -H "Accept: application/json" -H "Content-Type: multipart/form-data" \
  -H "Authorization: Bearer $token" -X POST -F "theme=@$(pwd)/dist/feneas.zip" \
  https://feneas.org/ghost/api/v0.1/themes/upload/

exit $?
