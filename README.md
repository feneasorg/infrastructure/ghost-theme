# Feneas Ghost Theme

(based on Casper Theme)

Use following docker image for development or production:

    docker run --rm -ti \
      -v $(pwd):/home/node/current/content/themes/casper \
      registry.git.feneas.org/feneas/infrastructure/ghost-theme:latest bash

## Build update package

    yarn install
    yarn zip

## Development

    cd ~ && ghost start && cd -
    yarn install
    yarn dev

Now you can start modifying files and visit the demo installation under `http://<docker-container-ip>:2368`!
